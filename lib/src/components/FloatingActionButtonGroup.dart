import 'package:flutter/material.dart';

class FloatingActionButtonGroup extends StatelessWidget{
  FloatingActionButtonGroup({Key key, this.leftButton, this.rightButton}) : super(key: key);

  final FloatingActionButton leftButton;
  final FloatingActionButton rightButton;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.bottomLeft,
          child: this.leftButton,
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: this.rightButton,
        ),
      ],
    );
  }
}
